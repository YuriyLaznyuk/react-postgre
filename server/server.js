require('dotenv').config();
const express = require('express');
const sequelize = require('./db');
const models = require('./models/models');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const path = require('path');
const router = require('./routes/index');
const errorHandler = require('../middleware/errorHandlingMiddleware');
const PORT = process.env.PORT || 8181;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static(path.resolve(__dirname, 'static')));
app.use(fileUpload({}));
app.use('/api', router);

//last middleware for error-handler
// end code
app.use(errorHandler);

const start = async () => {
  try {

    await sequelize.authenticate();
    await sequelize.sync();
    app.listen(PORT, () => {
      console.log(`server port ${PORT}`);
      console.log(path.resolve());
      console.log(path.resolve(__dirname,'static'));
      console.log(__dirname);
    });

  } catch (e) {
    console.log(e);
  }
};

start();


