 const ApiError=require('../../error/apiError')

class UserController {

    async registration(req,res){

    }
    async login(req,res){

    }

    async check(req,res,next){
        const params=req.query;
        if (!params.id){
          return next(ApiError.badRequest('id not found'));
        }
        res.json(params.id);

    }

}

module.exports=new UserController();